terraform {
  backend "s3" {
    bucket = "terraform-statefiles121234"
    key    = "state/terraform.tfstate"
    region = "us-east-1"
    access_key = ""
    secret_key = ""
  }
}

terraform {
  required_version = "~> 1.1.7"
}

provider "aws" {
  region     = "us-east-1"
  access_key = ""
  secret_key = ""

}



# module "amzn2" {
#   source = "./modules/amzn2"
# }

# module "ubun" {
#   source = "./modules/ubun"
#   #   tag_for_ec2 = "Test24"
# }

module "amzn2B" {
  source = "./modules/amzn2B"
  # instance_type = "t2.micro"
  amzn2b_name   = "Test"
  # vm_count      = 4
}


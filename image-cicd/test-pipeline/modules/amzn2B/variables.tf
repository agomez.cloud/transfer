variable "amzn2b_name" {
  type    = string
  default = "Test"
}

variable "vm_count" {
  type    = number
  default = 2

}
variable "instance_type" {
  type    = string
  default = "t2.micro"
}

data "aws_ami" "amzn" {
  most_recent = true
  owners      = ["137112412989"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

resource "aws_instance" "my-instance" {
  ami   = data.aws_ami.amzn.id
  count = var.vm_count
  #   ami           = "ami-0e1d30f2c40c4c701"
  instance_type = var.instance_type
  tags = {
    Name = "${var.amzn2b_name}-${count.index + 1}"
    # Name = "Test"
  }
}




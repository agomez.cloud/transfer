resource "aws_instance" "my-instance" {
  ami           = data.aws_ami.amzn.id
  instance_type = "t2.micro"

  tags = {
    Name = var.amzn2b_name
  }
}



resource "aws_instance" "my-instance" {
  ami           = "ami-04505e74c0741db8d"
  instance_type = var.type

  tags = {
    Name = var.tag_for_ec2
  }
}

# Variables will become the default values for this resource

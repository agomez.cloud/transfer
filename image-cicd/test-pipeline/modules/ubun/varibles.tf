variable "type" {
  type    = string
  default = "t2.micro"
}

variable "tag_for_ec2" {
  type    = string
  default = "ubun"
}

# The variables name will now be what is called 
# during the root main.tf

